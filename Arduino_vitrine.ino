//------------------------------------------------//
//                 ARDUINO_VITRINE                //
//                                                //
// Programme pour Arduino Leonardo                //
// controllant les modules de la vitrine:         //
//      • Capteur à ultrasons                     //
//      • Bandeau de leds                         //
// Lorsque le capteur à ultrasons detecte         //
// une distance plus faible que 30,               //
// le bandeau de leds est activé.                 //
//------------------------------------------------//
// Sources: 
//      • Bandeau de leds:   https://randomnerdtutorials.com/guide-for-ws2812b-addressable-rgb-led-strip-with-arduino/
//      • Capteur ultrasons: https://create.arduino.cc/projecthub/abdularbi17/ultrasonic-sensor-hc-sr04-with-arduino-tutorial-327ff6
//------------------------------------------------//
// Historique de modification: 
//      • 19/11/2021 - Création du script
//------------------------------------------------//

#define FASTLED_INTERNAL
// LEDS :
#include <FastLED.h>           // Librarie de control des leds

#define LED_PIN     5          // Pin de data pour les leds
#define NUM_LEDS    150        // Nombre de leds sur le ruban
#define BRIGHTNESS  64         // Luminosité
#define LED_TYPE    WS2811     // Type de ruban
#define COLOR_ORDER GRB        // Ordre des couleurs
CRGB leds[NUM_LEDS];
#define UPDATES_PER_SECOND 100 // Nombre de mises à jour par seconde

// ULTRASON:
#define ECHO_PIN 2 // attach pin D2 Arduino to pin Echo of HC-SR04
#define TRIG_PIN 3 // attach pin D3 Arduino to pin Trig of HC-SR04

#define WALL_DIST 10     // Après 30cm on a du mur
#define STATUE_DIST 8   // En dessous de 20cm on a la statuette
#define TRIGGER_COUNT 4 // Il faut que 10x consecutives le capteur detecte moins de 20cm

// This example shows several ways to set up and use 'palettes' of colors
// with FastLED.
//
// These compact palettes provide an easy way to re-colorize your
// animation on the fly, quickly, easily, and with low overhead.
//
// USING palettes is MUCH simpler in practice than in theory, so first just
// run this sketch, and watch the pretty lights as you then read through
// the code.  Although this sketch has eight (or more) different color schemes,
// the entire sketch compiles down to about 6.5K on AVR.
//
// FastLED provides a few pre-configured color palettes, and makes it
// extremely easy to make up your own color schemes with palettes.
//
// Some notes on the more abstract 'theory and practice' of
// FastLED compact palettes are at the bottom of this file.

// LEDS:
long duration;            // variable for the duration of sound wave travel
float tempDistance;       // Distance temporaire avant de faire la moyenne
int distanceCount;        // Nombre de distances temporaires avant de faire la moyenne
float distance = 100;     // variable for the distance measurement

bool trigger = false;     // si true, alors on passe du mode ultra au mode led
int triggerCount = 0;     // compteur du nombre de fois que la distance est réduite.


// ULTRASONS:
CRGBPalette16 currentPalette;
TBlendType    currentBlending;

extern CRGBPalette16 myRedWhiteBluePalette;
extern const TProgmemPalette16 myRedWhiteBluePalette_p PROGMEM;


void setup() {
    delay( 3000 );                                    // power-up safety delay

    //ULTRASONS:
    pinMode(TRIG_PIN, OUTPUT);                         // Sets the TRIG_PIN as an OUTPUT
    pinMode(ECHO_PIN, INPUT);                          // Sets the ECHO_PIN as an INPUT
    Serial.begin(9600);                               // Serial Communication is starting with 9600 of baudrate speed
    Serial.println("Ultrasonic Sensor HC-SR04 Test"); // print some text in Serial Monitor
    Serial.println("with Arduino UNO R3");

    //LEDS:
    FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
    FastLED.setBrightness(  BRIGHTNESS );
      FastLED.clear();  // clear all pixel data
  FastLED.show();

    currentPalette = RainbowColors_p;
    currentBlending = LINEARBLEND;
}


void loop()
{
    if(trigger)
      LedMode();
    else
      UltraSonicMode();
}

void LedMode()
{
    ChangePalettePeriodically();

    static uint8_t startIndex = 0;
    startIndex = startIndex + 1; /* motion speed */
    
    FillLEDsFromPaletteColors( startIndex);
    
    FastLED.show();
    FastLED.delay(1000 / UPDATES_PER_SECOND);
}

/* Pour considérer que la statuette est posée:
 *   • Si le capteur detecte une distance plus petite que STATUE_DIST:
 *      On ajoute 1 au compteur triggerCount
 *   • Si le capteur detecte une distance plus grande ou égale que WALL_DIST:
 *      On réinitialise le compteur triggerCount
 *   • Si le compteur est > TRIGGER_COUNT on active le trigger
 * 
 */
void UltraSonicMode()
{
    tempDistance += getDistance();
    distanceCount++;
    if(distanceCount > 100){

      distance = tempDistance / distanceCount;
      distanceCount = 0;
      tempDistance = 0;
      Serial.print("Distance: ");
      Serial.print(distance);
      Serial.println(" cm");
      
      if(distance <= STATUE_DIST)
        triggerCount++;
      else if (distance >= WALL_DIST)
        triggerCount = 0;
      if(triggerCount > TRIGGER_COUNT)
      {
        trigger = true;
        Serial.print("TRIGGER! : ");
        Serial.println(triggerCount);
      }
    }
    


}

int getDistance()
{

    // Clears the TRIG_PIN condition
    digitalWrite(TRIG_PIN, LOW);
    delayMicroseconds(2);
    // Sets the TRIG_PIN HIGH (ACTIVE) for 10 microseconds
    digitalWrite(TRIG_PIN, HIGH);
    delayMicroseconds(10);
    digitalWrite(TRIG_PIN, LOW);
    // Reads the ECHO_PIN, returns the sound wave travel time in microseconds
    duration = pulseIn(ECHO_PIN, HIGH);
    // Calculating the distance
    return duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)
  
}

void FillLEDsFromPaletteColors( uint8_t colorIndex)
{
    uint8_t brightness = 255;
    
    for( int i = 0; i < NUM_LEDS; i++) {
        leds[i] = ColorFromPalette( currentPalette, colorIndex, brightness, currentBlending);
        colorIndex += 3;
    }
}


// There are several different palettes of colors demonstrated here.
//
// FastLED provides several 'preset' palettes: RainbowColors_p, RainbowStripeColors_p,
// OceanColors_p, CloudColors_p, LavaColors_p, ForestColors_p, and PartyColors_p.
//
// Additionally, you can manually define your own color palettes, or you can write
// code that creates color palettes on the fly.  All are shown here.

void ChangePalettePeriodically()
{
    uint8_t secondHand = (millis() / 1000) % 60;
    static uint8_t lastSecond = 99;
    
    if( lastSecond != secondHand) {
        lastSecond = secondHand;
        if( secondHand ==  0)  { currentPalette = RainbowColors_p;         currentBlending = LINEARBLEND; }
        if( secondHand == 10)  { currentPalette = RainbowStripeColors_p;   currentBlending = NOBLEND;  }
        if( secondHand == 15)  { currentPalette = RainbowStripeColors_p;   currentBlending = LINEARBLEND; }
        if( secondHand == 20)  { SetupPurpleAndGreenPalette();             currentBlending = LINEARBLEND; }
        if( secondHand == 25)  { SetupTotallyRandomPalette();              currentBlending = LINEARBLEND; }
        if( secondHand == 30)  { SetupBlackAndWhiteStripedPalette();       currentBlending = NOBLEND; }
        if( secondHand == 35)  { SetupBlackAndWhiteStripedPalette();       currentBlending = LINEARBLEND; }
        if( secondHand == 40)  { currentPalette = CloudColors_p;           currentBlending = LINEARBLEND; }
        if( secondHand == 45)  { currentPalette = PartyColors_p;           currentBlending = LINEARBLEND; }
        if( secondHand == 50)  { currentPalette = myRedWhiteBluePalette_p; currentBlending = NOBLEND;  }
        if( secondHand == 55)  { currentPalette = myRedWhiteBluePalette_p; currentBlending = LINEARBLEND; }
    }
}

// This function fills the palette with totally random colors.
void SetupTotallyRandomPalette()
{
    for( int i = 0; i < 16; i++) {
        currentPalette[i] = CHSV( random8(), 255, random8());
    }
}

// This function sets up a palette of black and white stripes,
// using code.  Since the palette is effectively an array of
// sixteen CRGB colors, the various fill_* functions can be used
// to set them up.
void SetupBlackAndWhiteStripedPalette()
{
    // 'black out' all 16 palette entries...
    fill_solid( currentPalette, 16, CRGB::Black);
    // and set every fourth one to white.
    currentPalette[0] = CRGB::White;
    currentPalette[4] = CRGB::White;
    currentPalette[8] = CRGB::White;
    currentPalette[12] = CRGB::White;
    
}

// This function sets up a palette of purple and green stripes.
void SetupPurpleAndGreenPalette()
{
    CRGB purple = CHSV( HUE_PURPLE, 255, 255);
    CRGB green  = CHSV( HUE_GREEN, 255, 255);
    CRGB black  = CRGB::Black;
    
    currentPalette = CRGBPalette16(
                                   green,  green,  black,  black,
                                   purple, purple, black,  black,
                                   green,  green,  black,  black,
                                   purple, purple, black,  black );
}


// This example shows how to set up a static color palette
// which is stored in PROGMEM (flash), which is almost always more
// plentiful than RAM.  A static PROGMEM palette like this
// takes up 64 bytes of flash.
const TProgmemPalette16 myRedWhiteBluePalette_p PROGMEM =
{
    CRGB::Red,
    CRGB::Gray, // 'white' is too bright compared to red and blue
    CRGB::Blue,
    CRGB::Black,
    
    CRGB::Red,
    CRGB::Gray,
    CRGB::Blue,
    CRGB::Black,
    
    CRGB::Red,
    CRGB::Red,
    CRGB::Gray,
    CRGB::Gray,
    CRGB::Blue,
    CRGB::Blue,
    CRGB::Black,
    CRGB::Black
};



// Additionl notes on FastLED compact palettes:
//
// Normally, in computer graphics, the palette (or "color lookup table")
// has 256 entries, each containing a specific 24-bit RGB color.  You can then
// index into the color palette using a simple 8-bit (one byte) value.
// A 256-entry color palette takes up 768 bytes of RAM, which on Arduino
// is quite possibly "too many" bytes.
//
// FastLED does offer traditional 256-element palettes, for setups that
// can afford the 768-byte cost in RAM.
//
// However, FastLED also offers a compact alternative.  FastLED offers
// palettes that store 16 distinct entries, but can be accessed AS IF
// they actually have 256 entries; this is accomplished by interpolating
// between the 16 explicit entries to create fifteen intermediate palette
// entries between each pair.
//
// So for example, if you set the first two explicit entries of a compact 
// palette to Green (0,255,0) and Blue (0,0,255), and then retrieved 
// the first sixteen entries from the virtual palette (of 256), you'd get
// Green, followed by a smooth gradient from green-to-blue, and then Blue.
