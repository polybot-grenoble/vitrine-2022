# Vitrine 2022

Toutes les informations sur la vitrine, le code Arduino, les schemas électronique, dimensions, etc...

## Roadmap


- [ ] Faire fonctionner le code: Capteur déclenche les leds.

- [ ] Découper et assembler le plexiglass + tige filetée

- [ ] Intégrer l'électronique: Capteur, Arduino, bouton d'arret d'urgence, ruban de LEDs, zone statuette, batterie

- [ ] Ajuster le code pour la bonne distance

- [ ] Ajuster l'animation du ruban de LEDs

- [ ] Système 100% fiable et embarqué

## Tests

- [ ] Changement et chargement de la batterie

- [ ] Fiabilité du transport

- [ ] Bruits ambiants autour du capteur

- [ ] Longue durée d'alimentation
